#!/bin/bash -ex

# CONFIG
prefix="clan"
suffix=""
munki_package_name="clan"
display_name="CLAN"
icon_name=""
category="Scientific"
description="This update contains stability and security fixes for CLAN"
#url=`./finder.sh`

# download it (-L: follow redirects)
 curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36' http://dali.talkbank.org/clan/clan.dmg

# Obtain version info
# Mount disk image on temp space
# version=`defaults read "${plist}" version`
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`
echo Mounted on $mountpoint
pwd=`pwd`
/usr/sbin/pkgutil --expand "${mountpoint}/Install_Clan.pkg" pkg
mkdir build-root
(cd build-root; pax -rz -f ../pkg/clan.pkg/Payload)
cp -R "${mountpoint}/Install_Clan.pkg" app.pkg

version=`defaults read "${pwd}/build-root/CLAN/CLAN.app/Contents/Info.plist" CFBundleShortVersionString`
hdiutil detach "${mountpoint}"

# Build pkginfo
/usr/local/munki/makepkginfo app.pkg -f build-root/CLAN/CLAN.app  > app.plist
plist=`pwd`/app.plist

# Remove "build-root" from file paths
perl -p -i -e 's/build-root/\/Applications/' app.plist

# Remove redicuous string
#/usr/libexec/PlistBuddy -c "delete :installs:0:CFBundleVersion" app.plist

# TODO SET THIS and remove the exit -1
#defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
#defaults write "${plist}" unattended_install -bool YES
#echo 'TODO - configure for unattended install, if it can be'
#exit 42

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.9.0"
defaults write "${plist}" maximum_os_version "10.14.99"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist

# all done
